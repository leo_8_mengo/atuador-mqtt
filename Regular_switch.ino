#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Bounce2.h>
#include <time.h>



// Configuração do acesso ao Broker MQTT 
#define MQTT_AUTH false
#define MQTT_USERNAME ""
#define MQTT_PASSWORD ""

//Constantes
const String HOSTNAME = "teste";             
const char *OTA_PASSWORD = "admin"; 
              
const char *MQTT_LOG = "system/log";              
const char *MQTT_SYSTEM_CONTROL_TOPIC = "teste/system/set"; 
const char *MQTT_TEST_TOPIC = "teste/set"; 

const char *MQTT_SERVER = "192.168.1.105"; 

WiFiClient wclient;
PubSubClient client(MQTT_SERVER, 1883, wclient);
Bounce debouncer = Bounce();

//FLAGS de Controlo
bool OTA = false;
bool OTABegin = false;

int SwitchedPin = 16;
int interruptor = 5;
int lastState;



void setup() {
  Serial.begin(115200);
  WiFi.hostname(HOSTNAME);
 
  pinMode(SwitchedPin, OUTPUT);
  pinMode(interruptor, INPUT_PULLUP);
  
  debouncer.attach(interruptor);
  debouncer.interval(5);
  lastState = debouncer.read();

  digitalWrite(SwitchedPin, HIGH);

  WiFiManager wifiManager;
  //wifiManager.setAPCallback(switchStatus);
  wifiManager.setConfigPortalTimeout(180);
  wifiManager.autoConnect(HOSTNAME.c_str());
  //wifiManager.startConfigPortal(HOSTNAME.c_str());
  
  client.setCallback(callback); //Registo da função que vai responder ás mensagens vindos do MQTT 
}


//Chamada de recepção de mensagem
void callback(char *topic, byte *payload, unsigned int length) {
  String payloadStr = "";
  for (int i = 0; i < length; i++) {
    payloadStr += (char)payload[i];
  }
  String topicStr = String(topic);

  //controle do sistema
  if (topicStr.equals(MQTT_SYSTEM_CONTROL_TOPIC)) {
    if (payloadStr.equals("OTA_ON_" + String(HOSTNAME))) {
      Serial.println("OTA ON");
      client.publish(MQTT_LOG, "OTA ON");
      OTA = true;
      OTABegin = true;
    } else if (payloadStr.equals("OTA_OFF_" + String(HOSTNAME))) {
      Serial.println("OTA OFF");
      client.publish(MQTT_LOG, "OTA OFF");
      OTA = false;
      OTABegin = false;
    } else if (payloadStr.equals("REBOOT_" + String(HOSTNAME))) {
      Serial.println("REBOOT");
      client.publish(MQTT_LOG, "REBOOT");
      ESP.restart();
    }
    
  } 
  
  //Controle de comandos
  else if (topicStr.equals(MQTT_TEST_TOPIC)) {
    //TOPICO DE COMANDOS DE CONTROLE
     if(payloadStr.equals("ON")){
         Serial.println("ON");
         client.publish(MQTT_LOG, "ON");
         digitalWrite(SwitchedPin, LOW);
      }
     else if(payloadStr.equals("OFF")){
         Serial.println("OFF");
         client.publish(MQTT_LOG, "OFF");
         digitalWrite(SwitchedPin, HIGH);
      } 
  }
}

bool checkMqttConnection() {
  if (!client.connected()) {
    if (MQTT_AUTH ? client.connect(HOSTNAME.c_str(), MQTT_USERNAME, MQTT_PASSWORD) : client.connect(HOSTNAME.c_str())) {
      Serial.println("CONECTADO AO BROKER MQTT " + String(MQTT_SERVER));
      client.publish(MQTT_LOG, String("CONNECTED_" + HOSTNAME).c_str());
      //SUBSCRIÇÃO DE TOPICOS
      Serial.println(client.subscribe(MQTT_SYSTEM_CONTROL_TOPIC));
      client.subscribe(MQTT_TEST_TOPIC);
    }
    else {
      Serial.println("NÃO FOI POSSIVEL CONECTAR AO BROKER MQTT");
    }
  }

  return client.connected();
}

void loop() {

  if (WiFi.status() == WL_CONNECTED) { 
    if (checkMqttConnection()) {
      client.loop();
      if (OTA) {
       if (OTABegin) {
         setupOTA();        
         OTABegin = false;
       }
       ArduinoOTA.handle();     
     }
    }
  }

 debouncer.update();
 if(lastState != debouncer.read()) {
    lastState = debouncer.read();
    if(digitalRead(SwitchedPin)){
    digitalWrite(SwitchedPin, LOW);
    client.publish(MQTT_TEST_TOPIC,"ON");
 } 
 else{
     digitalWrite(SwitchedPin, HIGH);
     client.publish(MQTT_TEST_TOPIC,"OFF");
  }
 }
 
  
}

void setupOTA() {
  if (WiFi.status() == WL_CONNECTED && checkMqttConnection()) {
//    client.publish(MQTT_LOG, "OTA SETUP");
//    ArduinoOTA.setHostname(HOSTNAME.c_str());
//    ArduinoOTA.setPassword(OTA_PASSWORD);
    
    ArduinoOTA.onStart([]() {
    Serial.println("Start OTA");
    });
  
    ArduinoOTA.onEnd([]() {
      Serial.println("End OTA");
    });
  
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\n", (progress / (total / 100)));
    });
  
    ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
  
    ArduinoOTA.begin();
  
    Serial.println("Ready");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
}


